package to;

public class DepoimentoTO {

    private int idDepoimento;
    private int idUsuario;
    private String Comentario;


    public int getIdDepoimento() {
        return idDepoimento;
    }

    public void setIdDepoimento(int idDepoimento) {
        this.idDepoimento = idDepoimento;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getComentario() {
        return Comentario;
    }

    public void setComentario(String comentario) {
        Comentario = comentario;
    }
}
