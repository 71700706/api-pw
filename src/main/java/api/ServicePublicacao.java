package api;

import bo.PublicacaoBO;
import bo.UsuarioBO;
import to.PublicacaoTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("publicacao")
public class ServicePublicacao {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    public void insert(@HeaderParam("token") String token, PublicacaoTO model) throws Exception {
        if (UsuarioBO.isValid(token)) {
            PublicacaoBO.insert(model);
            response.sendError(HttpServletResponse.SC_CREATED);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes("application/json;charset=utf-8")
    public void delete(@HeaderParam("token") String token,@PathParam("id") int id) throws Exception {
        if (UsuarioBO.isValid(token)) {
            PublicacaoBO.delete(id);
            response.sendError(HttpServletResponse.SC_OK);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @GET
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public PublicacaoTO get(@HeaderParam("token") String token, @PathParam("id") int idPublicacao) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return PublicacaoBO.get(idPublicacao);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }

    @GET
    @Path("usuario/{idUsuario}")
    @Produces("application/json;charset=utf-8")
    public List<PublicacaoTO> getUsuario(@HeaderParam("token") String token, @PathParam("idUsuario") int idUsuario) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return PublicacaoBO.getUsuario(idUsuario);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }

    @GET
    @Path("feed/{idUsuario}")
    @Produces("application/json;charset=utf-8")
    public List<PublicacaoTO> getFeed(@HeaderParam("token") String token, @PathParam("idUsuario") int idUsuario) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return PublicacaoBO.getFeed(idUsuario);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }
}
