package bo;

import dao.DepoimentosDAO;
import dao.PublicacaoDAO;
import fw.Data;
import to.DepoimentoTO;
import to.PublicacaoTO;

import java.sql.Connection;
import java.util.List;

public class DepoimentoBO {

    public static void insert(DepoimentoTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            DepoimentosDAO.insert(c, model);
        }
    }
    public static DepoimentoTO get(int idPublicacao) throws Exception {
        try (Connection c = Data.openConnection()) {

            DepoimentoTO model = new DepoimentoTO();
            model.setIdDepoimento(idPublicacao);

            return DepoimentosDAO.get(c, model);
        }
    }
    public static List<DepoimentoTO> getUsuario(int idUsuario) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DepoimentosDAO.getUsuario(c, idUsuario);
        }
    }
}
