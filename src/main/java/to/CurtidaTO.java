package to;

import fw.DateTime;

import java.util.Date;

public class CurtidaTO {

    private int idCurtida;

    private int idPublicacao;
    private int idUsuarios;

    private Date Create;

    public int getIdCurtida() {
        return idCurtida;
    }

    public void setIdCurtida(int idCurtida) {
        this.idCurtida = idCurtida;
    }

    public int getIdPublicacao() {
        return idPublicacao;
    }

    public void setIdPublicacao(int idPublicacao) {
        this.idPublicacao = idPublicacao;
    }

    public int getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(int idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public Date getCreate() {
        return Create;
    }

    public void setCreate(Date create) {
        Create = create;
    }
}
