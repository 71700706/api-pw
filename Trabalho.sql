
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

DROP DATABASE WEB;
CREATE DATABASE WEB;
USE WEB;


CREATE TABLE `Usuarios` (
  `idUsuarios` INT NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(45) NULL,
  `Nascimento` DATETIME NULL,
  `Email` VARCHAR(120) NULL,
  `Password` VARCHAR(45) NULL,
  `Token` VARCHAR(40) NULL,
  `Active` TINYINT NULL,
  `Createdat` TIMESTAMP NULL,
  `Expiredat` TIMESTAMP NULL,
  PRIMARY KEY (`idUsuarios`))
ENGINE = InnoDB;


CREATE TABLE `Publicacoes` (
  `idPublicacao` INT NOT NULL AUTO_INCREMENT,
  `idUsuario` INT NOT NULL,
  `Publicacao` TEXT NULL,
  `Created` DATETIME NULL,
  PRIMARY KEY (`idPublicacao`, `idUsuario`),
  CONSTRAINT `fk_Publicacoes_Usuarios1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `Usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE `Curtidas` (
  `idCurtida` INT NOT NULL AUTO_INCREMENT,
  `Created` DATETIME NULL,
  `idPublicacao` INT NOT NULL,
  `idUsuarios` INT NOT NULL,
  PRIMARY KEY (`idCurtida`),
  CONSTRAINT `fk_Curtidas_Publicacoes1`
    FOREIGN KEY (`idPublicacao`)
    REFERENCES `Publicacoes` (`idPublicacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Curtidas_Usuarios1`
    FOREIGN KEY (`idUsuarios`)
    REFERENCES `Usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE `Comentarios` (
  `idComentario` INT NOT NULL AUTO_INCREMENT,
  `idPublicacao` INT NOT NULL,
  `idUsuarios` INT NOT NULL,
  `Comentario` TEXT NULL,
  `Created` DATETIME NULL,
  PRIMARY KEY (`idComentario`),
  CONSTRAINT `fk_Comentarios_Publicacoes1`
    FOREIGN KEY (`idPublicacao`)
    REFERENCES `Publicacoes` (`idPublicacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comentarios_Usuarios1`
    FOREIGN KEY (`idUsuarios`)
    REFERENCES `Usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE `Anexos` (
  `idAnexo` INT NOT NULL AUTO_INCREMENT,
  `idPublicacao` INT NOT NULL,
  `arquivo` BLOB NULL,
  PRIMARY KEY (`idAnexo`, `idPublicacao`),
  CONSTRAINT `fk_Anexos_Publicacoes1`
    FOREIGN KEY (`idPublicacao`)
    REFERENCES `Publicacoes` (`idPublicacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE `Amigos` (
  `idAmigo` INT NOT NULL AUTO_INCREMENT,
  `Created` DATETIME NULL,
  `idUsuario` INT NOT NULL,
  `Amigo` INT NOT NULL,
  PRIMARY KEY (`idAmigo`),
  CONSTRAINT `fk_Amigos_Usuarios1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `Usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Amigos_Usuarios2`
    FOREIGN KEY (`idAmigo`)
    REFERENCES `Usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE `Bloqueados` (
  `idBloqueados` INT NOT NULL AUTO_INCREMENT,
  `Created` DATETIME NULL,
  `idUsuario` INT NOT NULL,
  `idBloqueado` INT NOT NULL,
  PRIMARY KEY (`idBloqueados`),
  CONSTRAINT `fk_Bloqueados_Usuarios1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `Usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bloqueados_Usuarios2`
    FOREIGN KEY (`idBloqueado`)
    REFERENCES `Usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE `Depoimentos` (
  `idDepoimento` INT NOT NULL AUTO_INCREMENT,
  `idUsuario` INT NOT NULL,
  `Comentario` TEXT NULL,
  PRIMARY KEY (`idDepoimento`),
  CONSTRAINT `fk_Depoimentos_Usuarios1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `Usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
