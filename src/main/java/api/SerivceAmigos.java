package api;

import bo.UsuarioBO;
import bo.AmigosBO;
import to.AmigosTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("amigos")
public class ServiceAmigos {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    public void insert(@HeaderParam("token") String token, AmigosTO model) throws Exception {
        if (AmigosBO.isValid(token)) {
            AmigosBO.insert(model);
            response.sendError(HttpServletResponse.SC_CREATED);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes("application/json;charset=utf-8")
    public void delete(@HeaderParam("token") String token,@PathParam("id") int id) throws Exception {
        if (UsuarioBO.isValid(token)) {
            AmigosBO.delete(id);
            response.sendError(HttpServletResponse.SC_OK);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @GET
    @Path("{id}")
    public static AmigosTO get(@HeaderParam("token") String token, int id) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return AmigosBO.get(id);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }

    @GET
    @Path("feed/{idUsuario}")
    @Produces("application/json;charset=utf-8")
    public List<PublicacaoTO> getAmigos(@HeaderParam("token") String token, @PathParam("idUsuario") int idUsuario) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return AmigosBO.getAmigos(idUsuario);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }

}
