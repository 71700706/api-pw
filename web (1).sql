-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 12-Jun-2019 às 21:27
-- Versão do servidor: 5.5.61
-- versão do PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `web`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `amigos`
--

CREATE TABLE `amigos` (
  `idAmigo` int(11) NOT NULL,
  `Created` datetime DEFAULT NULL,
  `idUsuario` int(11) NOT NULL,
  `Amigo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `anexos`
--

CREATE TABLE `anexos` (
  `idAnexo` int(11) NOT NULL,
  `idPublicacao` int(11) NOT NULL,
  `arquivo` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bloqueados`
--

CREATE TABLE `bloqueados` (
  `idBloqueados` int(11) NOT NULL,
  `Created` datetime DEFAULT NULL,
  `idUsuario` int(11) NOT NULL,
  `idBloqueado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE `comentarios` (
  `idComentario` int(11) NOT NULL,
  `idPublicacao` int(11) NOT NULL,
  `idUsuarios` int(11) NOT NULL,
  `Comentario` text,
  `Created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `comentarios`
--

INSERT INTO `comentarios` (`idComentario`, `idPublicacao`, `idUsuarios`, `Comentario`, `Created`) VALUES
(1, 3, 3, 'Odeio isso tudo', '2019-06-12 00:00:00'),
(2, 3, 3, 'Odeio isso tudo', '2019-06-12 16:24:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `curtidas`
--

CREATE TABLE `curtidas` (
  `idCurtida` int(11) NOT NULL,
  `Created` datetime DEFAULT NULL,
  `idPublicacao` int(11) NOT NULL,
  `idUsuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `curtidas`
--

INSERT INTO `curtidas` (`idCurtida`, `Created`, `idPublicacao`, `idUsuarios`) VALUES
(1, '2019-06-12 00:00:00', 3, 3),
(2, '2019-06-12 16:03:39', 3, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `depoimentos`
--

CREATE TABLE `depoimentos` (
  `idDepoimento` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `Comentario` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacoes`
--

CREATE TABLE `publicacoes` (
  `idPublicacao` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `Publicacao` text,
  `Active` tinyint(1) NOT NULL,
  `Created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `publicacoes`
--

INSERT INTO `publicacoes` (`idPublicacao`, `idUsuario`, `Publicacao`, `Active`, `Created`) VALUES
(1, 3, 'Nada de Mais Cara', 1, NULL),
(2, 3, 'Por post Nada lknlknlnknkjbkjbjbjhbjbbbjbjhbjbhjbjbhbhbjbjjhbhjbjhb', 1, '2019-06-12 15:38:55'),
(3, 3, 'Hoje nao sata', 0, '2019-06-12 15:44:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuarios` int(11) NOT NULL,
  `Nome` varchar(45) DEFAULT NULL,
  `Nascimento` datetime DEFAULT NULL,
  `Email` varchar(120) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `Token` varchar(40) DEFAULT NULL,
  `Active` tinyint(4) DEFAULT NULL,
  `Createdat` timestamp NULL DEFAULT NULL,
  `Expiredat` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`idUsuarios`, `Nome`, `Nascimento`, `Email`, `Password`, `Token`, `Active`, `Createdat`, `Expiredat`) VALUES
(2, 'Rafael Marques 1', '2016-12-31 22:00:00', 'rafaelmarques111@hotmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '7dobs0q8934gh1dd6iflt5', 1, '2019-06-12 17:10:10', '2019-06-12 20:26:25'),
(3, 'Rafael Marques 1', '2016-12-31 22:00:00', 'rafaelmarques111@hotmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, 1, '2019-06-12 17:37:22', NULL),
(4, 'Rafael Marques 1', '2016-12-31 22:00:00', 'rafaelmarques111@hotmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, 1, '2019-06-12 18:25:06', NULL),
(5, 'Rafael Marques 1', '2016-12-31 22:00:00', 'rafaelmarques111@hotmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, 1, '2019-06-12 18:25:07', NULL);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `amigos`
--
ALTER TABLE `amigos`
  ADD PRIMARY KEY (`idAmigo`),
  ADD KEY `fk_Amigos_Usuarios1` (`idUsuario`);

--
-- Índices para tabela `anexos`
--
ALTER TABLE `anexos`
  ADD PRIMARY KEY (`idAnexo`,`idPublicacao`),
  ADD KEY `fk_Anexos_Publicacoes1` (`idPublicacao`);

--
-- Índices para tabela `bloqueados`
--
ALTER TABLE `bloqueados`
  ADD PRIMARY KEY (`idBloqueados`),
  ADD KEY `fk_Bloqueados_Usuarios1` (`idUsuario`),
  ADD KEY `fk_Bloqueados_Usuarios2` (`idBloqueado`);

--
-- Índices para tabela `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idComentario`),
  ADD KEY `fk_Comentarios_Publicacoes1` (`idPublicacao`),
  ADD KEY `fk_Comentarios_Usuarios1` (`idUsuarios`);

--
-- Índices para tabela `curtidas`
--
ALTER TABLE `curtidas`
  ADD PRIMARY KEY (`idCurtida`),
  ADD KEY `fk_Curtidas_Publicacoes1` (`idPublicacao`),
  ADD KEY `fk_Curtidas_Usuarios1` (`idUsuarios`);

--
-- Índices para tabela `depoimentos`
--
ALTER TABLE `depoimentos`
  ADD PRIMARY KEY (`idDepoimento`),
  ADD KEY `fk_Depoimentos_Usuarios1` (`idUsuario`);

--
-- Índices para tabela `publicacoes`
--
ALTER TABLE `publicacoes`
  ADD PRIMARY KEY (`idPublicacao`,`idUsuario`),
  ADD KEY `fk_Publicacoes_Usuarios1` (`idUsuario`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuarios`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `amigos`
--
ALTER TABLE `amigos`
  MODIFY `idAmigo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `anexos`
--
ALTER TABLE `anexos`
  MODIFY `idAnexo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `bloqueados`
--
ALTER TABLE `bloqueados`
  MODIFY `idBloqueados` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `idComentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `curtidas`
--
ALTER TABLE `curtidas`
  MODIFY `idCurtida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `depoimentos`
--
ALTER TABLE `depoimentos`
  MODIFY `idDepoimento` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `publicacoes`
--
ALTER TABLE `publicacoes`
  MODIFY `idPublicacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `amigos`
--
ALTER TABLE `amigos`
  ADD CONSTRAINT `fk_Amigos_Usuarios1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Amigos_Usuarios2` FOREIGN KEY (`idAmigo`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `anexos`
--
ALTER TABLE `anexos`
  ADD CONSTRAINT `fk_Anexos_Publicacoes1` FOREIGN KEY (`idPublicacao`) REFERENCES `publicacoes` (`idPublicacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `bloqueados`
--
ALTER TABLE `bloqueados`
  ADD CONSTRAINT `fk_Bloqueados_Usuarios1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Bloqueados_Usuarios2` FOREIGN KEY (`idBloqueado`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `fk_Comentarios_Publicacoes1` FOREIGN KEY (`idPublicacao`) REFERENCES `publicacoes` (`idPublicacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Comentarios_Usuarios1` FOREIGN KEY (`idUsuarios`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `curtidas`
--
ALTER TABLE `curtidas`
  ADD CONSTRAINT `fk_Curtidas_Publicacoes1` FOREIGN KEY (`idPublicacao`) REFERENCES `publicacoes` (`idPublicacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Curtidas_Usuarios1` FOREIGN KEY (`idUsuarios`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `depoimentos`
--
ALTER TABLE `depoimentos`
  ADD CONSTRAINT `fk_Depoimentos_Usuarios1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `publicacoes`
--
ALTER TABLE `publicacoes`
  ADD CONSTRAINT `fk_Publicacoes_Usuarios1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
