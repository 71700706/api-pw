package dao;

import fw.Data;
import to.ComentarioTO;
import to.CurtidaTO;
import to.PublicacaoTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CurtidasDAO {

    public static void insert(Connection c, CurtidaTO model) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO `Curtidas`(`Created`, `idPublicacao`, `idUsuarios`) VALUES ");
        sql.append(" (NOW(), ?, ?) ");

        Data.executeUpdate(c, sql.toString(), model.getIdPublicacao(), model.getIdUsuarios());

    }
    public static void delete(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM `Curtidas` WHERE `idCurtida` = ?");

        Data.executeUpdate(c, sql.toString(), id);
    }


    public static CurtidaTO get(Connection c, CurtidaTO find) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idCurtida`, `Created`, `idPublicacao`, `idUsuarios` FROM `Curtidas`");
        sql.append(" where ");
        sql.append(" idCurtida = ? ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), find.getIdCurtida())){

            if(rs.next()){

                CurtidaTO model = new CurtidaTO();

                model.setIdCurtida(rs.getInt("idCurtida"));
                model.setIdPublicacao(rs.getInt("idPublicacao"));
                model.setIdUsuarios(rs.getInt("idUsuarios"));
                model.setCreate(rs.getDate("Created"));

                return model;

            }else{
                return null;
            }

        }
    }
    // Obter todos comentarios
    public static List<CurtidaTO> getCurtidas(Connection c, PublicacaoTO find) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idCurtida`, `Created`, `idPublicacao`, `idUsuarios` FROM `Curtidas` ");
        sql.append(" where ");
        sql.append(" idPublicacao = ? ");


        try(ResultSet rs = Data.executeQuery(c, sql.toString(), find.getIdPublicacao())){

            List<CurtidaTO> result = new ArrayList<>();

            while(rs.next()){
                CurtidaTO model = new CurtidaTO();

                model.setIdCurtida(rs.getInt("idCurtida"));
                model.setIdPublicacao(rs.getInt("idPublicacao"));
                model.setIdUsuarios(rs.getInt("idUsuarios"));
                model.setCreate(rs.getDate("Created"));

                result.add(model);
            }

            return result;
        }
    }

}
