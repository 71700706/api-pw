package api;

import bo.UsuarioBO;
import to.UsuarioTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("usuario")
public class ServiceUsuario {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    public void insert(UsuarioTO user) throws Exception {
        UsuarioBO.insert(user);
        response.sendError(HttpServletResponse.SC_CREATED);
    }

    @PUT
    @Consumes("application/json;charset=utf-8")
    public void update(@HeaderParam("token") String token, UsuarioTO user) throws Exception {
        if (UsuarioBO.update(user)) {
            response.sendError(HttpServletResponse.SC_ACCEPTED);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @POST
    @Path("auth")
    @Produces("application/json;charset=utf-8")
    public UsuarioTO auth(UsuarioTO usuario) throws Exception {
        UsuarioTO user = UsuarioBO.auth(usuario);
        if (user != null) {
            return user;
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }

    @GET
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public UsuarioTO get(@PathParam("id") String id) throws Exception {
        UsuarioTO user = UsuarioBO.get(id);
        if (user != null) {
            return user;
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @GET
    @Path("token")
    @Produces("application/json;charset=utf-8")
    public UsuarioTO getToken(@HeaderParam("token") String token) throws Exception {
        UsuarioTO user = UsuarioBO.getByToken(token);
        if (user != null) {
            return user;
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @GET
    @Produces("application/json;charset=utf-8")
    public List<UsuarioTO> all() throws Exception {
        List<UsuarioTO> usuarios = UsuarioBO.getAll();
        if (usuarios == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        return usuarios;
    }
}
