package bo;

import dao.AnexosDAO;
import dao.CurtidasDAO;
import fw.Data;
import to.AnexoTO;
import to.CurtidaTO;
import to.PublicacaoTO;

import java.sql.Connection;
import java.util.List;

public class AnexosBO {

    public static void insert(AnexoTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            AnexosDAO.insert(c, model);
        }
    }
    public static List<AnexoTO> get(PublicacaoTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            return AnexosDAO.get(c, model);
        }
    }
}
