package bo;

import dao.ComentarioDAO;
import dao.CurtidasDAO;
import fw.Data;
import to.ComentarioTO;
import to.CurtidaTO;
import to.PublicacaoTO;

import java.sql.Connection;
import java.util.List;

public class CurtidasBO {

    public static void insert(CurtidaTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            CurtidasDAO.insert(c, model);
        }
    }
    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            CurtidasDAO.delete(c, id);
        }
    }
    public static CurtidaTO get(int idCurtida) throws Exception {
        try (Connection c = Data.openConnection()) {

            CurtidaTO model = new CurtidaTO();
            model.setIdCurtida(idCurtida);
            return CurtidasDAO.get(c, model);
        }
    }
    public static List<CurtidaTO> getCurtidas(int idPublicacao) throws Exception {
        try (Connection c = Data.openConnection()) {

            PublicacaoTO model = new PublicacaoTO();
            model.setIdPublicacao(idPublicacao);
            return CurtidasDAO.getCurtidas(c, model);
        }
    }
}
