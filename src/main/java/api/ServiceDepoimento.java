package api;

import bo.ComentarioBO;
import bo.DepoimentoBO;
import to.ComentarioTO;
import to.DepoimentoTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("depoimento")
public class ServiceDepoimento {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    public void insert(@HeaderParam("token") String token, DepoimentoTO model) throws Exception {
        if (UsuarioBO.isValid(token)) {
            DepoimentoBO.insert(model);
            response.sendError(HttpServletResponse.SC_CREATED);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }        
    }

    @GET
    @Path("{id}")
    @Consumes("application/json;charset=utf-8")
    public static DepoimentoTO get(@HeaderParam("token") String token, @PathParam("id") int id) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return DepoimentoBO.get(id);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }
    @GET
    @Path("Usuario/{idPublicacao}")
    public static List<DepoimentoTO> getUsuario(@HeaderParam("token") String token, int idPublicacao) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return DepoimentoBO.getUsuario(idPublicacao);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
        

    }
}
