package bo;

import dao.UsuarioDAO;
import fw.Data;
import fw.DateTime;
import fw.Encrypt;
import fw.Guid;
import to.UsuarioTO;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

public class UsuarioBO {

    public static boolean isValid(String token) throws Exception {
        try (Connection c = Data.openConnection()) {
            UsuarioTO a = UsuarioDAO.getByToken(c, token);
            if (a != null) {

                DateTime now = DateTime.now();
                if (a.getExpiredAt().getTime() > now.getMillis()) {
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        }
    }

    public static UsuarioTO auth(UsuarioTO u) throws Exception {
        try (Connection c = Data.openConnection()) {
            u.setPassword(Encrypt.sha1(u.getPassword()));
            UsuarioTO t = UsuarioDAO.auth(c, u);
            if (t != null) {
                DateTime expiredAt = DateTime.now();
                expiredAt.addMinute(60);
                t.setExpiredAt(expiredAt.getTimestamp());
                t.setToken(Guid.getString());
                UsuarioDAO.updateToken(c, t);
            }
            return t;
        }
    }

    public static void insert(UsuarioTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            model.setPassword(Encrypt.sha1(model.getPassword()));
            UsuarioDAO.insert(c, model);
        }
    }

    public static List<UsuarioTO>  getAll() throws Exception {
        try (Connection c = Data.openConnection()) {
            return UsuarioDAO.getAll(c);
        }
    }



    public static UsuarioTO getByToken(String token) throws Exception {
        try (Connection c = Data.openConnection()) {
            return UsuarioDAO.getByToken(c, token);
        }
    }

    public static UsuarioTO get(String id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return UsuarioDAO.get(c, id);
        }
    }

    public static Boolean update(UsuarioTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            UsuarioDAO.update(c, model);
            return true;
        }
    }

    public static void delete(UsuarioTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            UsuarioDAO.delete(c, model);
        }
    }
}
