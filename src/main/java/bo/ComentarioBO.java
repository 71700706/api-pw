package bo;

import dao.ComentarioDAO;
import dao.PublicacaoDAO;
import fw.Data;
import to.ComentarioTO;
import to.PublicacaoTO;

import java.sql.Connection;
import java.util.List;

public class ComentarioBO {

    public static void insert(ComentarioTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            ComentarioDAO.insert(c, model);
        }
    }
    public static void delete(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            ComentarioDAO.delete(c, id);
        }
    }
    public static ComentarioTO get(int idComentario) throws Exception {
        try (Connection c = Data.openConnection()) {
            ComentarioTO model = new ComentarioTO();
            model.setIdComentario(idComentario);
            return ComentarioDAO.get(c, model);
        }
    }
    public static List<ComentarioTO> getComentarios(int idPublicacao) throws Exception {
        try (Connection c = Data.openConnection()) {

            PublicacaoTO model = new PublicacaoTO();
            model.setIdPublicacao(idPublicacao);
            return ComentarioDAO.getComentarios(c, model);
        }
    }
}
