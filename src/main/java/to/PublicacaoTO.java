package to;

import fw.DateTime;

import java.util.Date;
import java.util.List;

public class PublicacaoTO {

    private int idPublicacao;
    private int idUsuario;

    private String Publicacao;
    private Boolean Active;
    private Date Create;

    public int getIdPublicacao() {
        return idPublicacao;
    }

    public void setIdPublicacao(int idPublicacao) {
        this.idPublicacao = idPublicacao;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getPublicacao() {
        return Publicacao;
    }

    public void setPublicacao(String publicacao) {
        Publicacao = publicacao;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public Date getCreate() {
        return Create;
    }

    public void setCreate(Date create) {
        Create = create;
    }
}
