package dao;

import fw.Data;
import to.AnexoTO;
import to.ComentarioTO;
import to.CurtidaTO;
import to.PublicacaoTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AnexosDAO {

    public static void insert(Connection c, AnexoTO model) throws Exception {

        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO `Anexos`(`idPublicacao`, `arquivo`)  VALUES ");
        sql.append(" (?, ?, ?) ");

        Data.executeUpdate(c, sql.toString(), model.getIdPublicacao(), model.getArquivo());

    }

    // Obter todos anexos da publicacao
    public static List<AnexoTO> get(Connection c, PublicacaoTO find) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idAnexo`, `idPublicacao`, `arquivo` FROM `Anexos` ");
        sql.append(" where ");
        sql.append(" idPublicacao = ? and active ");


        try(ResultSet rs = Data.executeQuery(c, sql.toString(), find.getIdPublicacao())){

            List<AnexoTO> result = new ArrayList<>();

            while(rs.next()){
                AnexoTO model = new AnexoTO();

                model.setIdPublicacao(rs.getInt("idAnexo"));
                model.setIdPublicacao(rs.getInt("idPublicacao"));
                model.setIdAnexo(rs.getByte("arquivo"));

                result.add(model);
            }

            return result;
        }
    }

}
