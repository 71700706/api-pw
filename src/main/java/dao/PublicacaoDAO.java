package dao;
import fw.Data;
import fw.DateTime;
import to.PublicacaoTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PublicacaoDAO {

    public static void insert(Connection c, PublicacaoTO model) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO `Publicacoes`(`idUsuario`, `Publicacao`, `Created`, `Active`) VALUES");
        sql.append(" (?, ?, NOW(), 1) ");

        Data.executeUpdate(c, sql.toString(), model.getIdUsuario(), model.getPublicacao());
    }
    public static void delete(Connection c, PublicacaoTO model) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE `Publicacoes` SET Active = 0 WHERE `idPublicacao` = ?");
        Data.executeUpdate(c, sql.toString(), model.getIdPublicacao());
    }

    public static PublicacaoTO get(Connection c, PublicacaoTO model) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idPublicacao`, `idUsuario`, `Publicacao`, `Active`, `Created` FROM `Publicacoes`");
        sql.append(" where ");
        sql.append(" idPublicacao = ? and Active ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), model.getIdPublicacao())){
            if(rs.next()){
                PublicacaoTO result = setObject(rs);
                return result;
            }else{
                return null;
            }
        }
    }

    public static List<PublicacaoTO> getUsuario(Connection c, int idUsuario) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idPublicacao`, `idUsuario`, `Publicacao`, `Active`, `Created` FROM `Publicacoes`" );
        sql.append(" where ");
        sql.append(" idUsuario = ? and Active ");
        sql.append(" order by Created ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), idUsuario)){

            List<PublicacaoTO> result = new ArrayList<>();
            while(rs.next()){
                PublicacaoTO model = setObject(rs);
                result.add(model);
            }

            return result;
        }
    }

    public static List<PublicacaoTO> getFeed(Connection c, int idUsuario) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idPublicacao`, `idUsuario`, `Publicacao`, `Active`, `Created` FROM `Publicacoes`" );
        sql.append(" where ");
        sql.append(" idUsuario = ? and Active ");
        sql.append(" order by Created ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), idUsuario)){

            List<PublicacaoTO> result = new ArrayList<>();
            while(rs.next()){
                PublicacaoTO model = setObject(rs);
                result.add(model);
            }

            return result;
        }
    }

    private static PublicacaoTO setObject(ResultSet rs) throws Exception {
        PublicacaoTO model = new PublicacaoTO();
        model.setIdPublicacao(rs.getInt("idPublicacao"));
        model.setIdUsuario(rs.getInt("idUsuario"));
        model.setPublicacao(rs.getString("Publicacao"));
        model.setActive(rs.getBoolean("Active"));
        model.setCreate(rs.getDate("Created"));

        return model;
    }
}
