package bo;

import dao.PublicacaoDAO;
import fw.Data;
import to.PublicacaoTO;

import java.sql.Connection;
import java.util.List;

public class PublicacaoBO {

    public static void insert(PublicacaoTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            PublicacaoDAO.insert(c, model);
        }
    }
    public static void delete(int idPublicacao) throws Exception {
        try (Connection c = Data.openConnection()) {
            PublicacaoTO model = new PublicacaoTO();
            model.setIdPublicacao(idPublicacao);
            PublicacaoDAO.delete(c, model);
        }
    }
    public static PublicacaoTO get(int idPublicacao) throws Exception {
        try (Connection c = Data.openConnection()) {
            PublicacaoTO model = new PublicacaoTO();
            model.setIdPublicacao(idPublicacao);
            return PublicacaoDAO.get(c, model);
        }
    }
    public static List<PublicacaoTO> getUsuario(int idUsuario) throws Exception {
        try (Connection c = Data.openConnection()) {
            return PublicacaoDAO.getUsuario(c, idUsuario);
        }
    }
    public static List<PublicacaoTO> getFeed(int idUsuario) throws Exception {
        try (Connection c = Data.openConnection()) {
            return PublicacaoDAO.getFeed(c, idUsuario);
        }
    }
}
