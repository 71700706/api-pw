package to;

import java.util.Date;

public class ComentarioTO {

    private int idComentario;

    private int idPublicacao;
    private int idUsuarios;

    private String Comentario;


    private Date Create;

    public Date getCreate() {
        return Create;
    }

    public void setCreate(Date create) {
        Create = create;
    }

    public int getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(int idComentario) {
        this.idComentario = idComentario;
    }

    public int getIdPublicacao() {
        return idPublicacao;
    }

    public void setIdPublicacao(int idPublicacao) {
        this.idPublicacao = idPublicacao;
    }

    public int getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(int idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public String getComentario() {
        return Comentario;
    }

    public void setComentario(String comentario) {
        Comentario = comentario;
    }
}
