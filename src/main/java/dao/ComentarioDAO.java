package dao;

import fw.Data;
import fw.DateTime;
import to.ComentarioTO;
import to.PublicacaoTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ComentarioDAO {

    public static void insert(Connection c, ComentarioTO model) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO `Comentarios`(`idPublicacao`, `idUsuarios`, `Comentario`, `Created`) VALUES ");
        sql.append(" (?, ?, ?, NOW()) ");

        Data.executeUpdate(c, sql.toString(), model.getIdPublicacao(), model.getIdUsuarios(), model.getComentario());

    }
    public static void delete(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM `Comentarios` WHERE `idComentario` = ?");

        Data.executeUpdate(c, sql.toString(), id);
    }

    public static ComentarioTO get(Connection c, ComentarioTO find) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idComentario`, `idPublicacao`, `idUsuarios`, `Comentario`, `Created` FROM `Comentarios` ");
        sql.append(" where ");
        sql.append(" idComentario = ? ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), find.getIdComentario())){

            if(rs.next()){

                ComentarioTO model = new ComentarioTO();
                model.setIdPublicacao(rs.getInt("idPublicacao"));
                model.setIdComentario(rs.getInt("idComentario"));
                model.setIdUsuarios(rs.getInt("idUsuarios"));
                model.setComentario(rs.getString("Comentario"));
                model.setCreate(rs.getTimestamp("Created"));

                return model;

            }else{
                return null;
            }

        }
    }

    // Obter todos comentarios
    public static List<ComentarioTO> getComentarios(Connection c, PublicacaoTO find) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idComentario`, `idPublicacao`, `idUsuarios`, `Comentario`, `Created` FROM `Comentarios` ");
        sql.append(" where ");
        sql.append(" idPublicacao = ? ");


        try(ResultSet rs = Data.executeQuery(c, sql.toString(), find.getIdPublicacao())){

            List<ComentarioTO> result = new ArrayList<>();

            while(rs.next()){
                ComentarioTO model = new ComentarioTO();

                model.setIdPublicacao(rs.getInt("idPublicacao"));
                model.setIdComentario(rs.getInt("idComentario"));
                model.setIdUsuarios(rs.getInt("idUsuarios"));
                model.setComentario(rs.getString("Comentario"));
                model.setCreate(rs.getTimestamp("Created"));

                result.add(model);

            }

            return result;
        }
    }

}
