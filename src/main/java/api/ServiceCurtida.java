package api;

import bo.ComentarioBO;
import bo.CurtidasBO;
import bo.PublicacaoBO;
import bo.UsuarioBO;
import to.ComentarioTO;
import to.CurtidaTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("curtida")
public class ServiceCurtida {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    public void insert(@HeaderParam("token") String token, CurtidaTO model) throws Exception {
        if (UsuarioBO.isValid(token)) {
            CurtidasBO.insert(model);
            response.sendError(HttpServletResponse.SC_CREATED);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes("application/json;charset=utf-8")
    public void delete(@HeaderParam("token") String token, @PathParam("id")int id) throws Exception {
        if (UsuarioBO.isValid(token)) {
            CurtidasBO.delete(id);
            response.sendError(HttpServletResponse.SC_OK);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @GET
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public CurtidaTO get(@HeaderParam("token") String token, @PathParam("id")int id) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return CurtidasBO.get(id);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }
    @GET
    @Path("publicacao/{idPublicacao}")
    @Produces("application/json;charset=utf-8")
    public List<CurtidaTO> getUsuario(@HeaderParam("token") String token, @PathParam("idPublicacao") int idPublicacao) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return CurtidasBO.getCurtidas(idPublicacao);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }


}
