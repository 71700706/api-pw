package api;

import bo.UsuarioBO;
import bo.AnexosBO;
import bo.DepoimentoBO;
import to.AnexoTO;
import to.DepoimentoTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("anexos")
public class ServiceAnexos {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    public void insert(@HeaderParam("token") String token, AnexoTO model) throws Exception {
        if (UsuarioBO.isValid(token)) {
            AnexosBO.insert(model);
            response.sendError(HttpServletResponse.SC_CREATED);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @GET
    @Path("{id}")
    public static AnexoTO get(@HeaderParam("token") String token, int id) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return AnexosBO.get(id);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }
}
