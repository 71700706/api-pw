package dao;

import fw.Data;
import to.PublicacaoTO;
import to.UsuarioTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO {

    public static void insert(Connection conn, UsuarioTO user) throws Exception {
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO `Usuarios`(`Nome`, `Nascimento`, `Email`, `Password`, `Active`, `Createdat`) VALUES ");
        sql.append("(?, ?, ?, ?, TRUE, NOW()); ");

        Data.executeUpdate(conn, sql.toString(), user.getNome(), user.getNascimento(), user.getEmail(), user.getPassword());
    }

    public static void update(Connection conn, UsuarioTO user) throws Exception {
        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE `Usuarios` SET ");
        sql.append(" `Nome` = ?, ");
        sql.append(" `Nascimento` = ?, ");
        sql.append(" `Email` = ?, ");
        sql.append(" `Password` = ?, ");
        sql.append(" `Active` = ?, ");
        sql.append(" `Expiredat` = ?, ");
        sql.append("  WHERE `idUsuario` = ?; ");

        Data.executeUpdate(conn, sql.toString(), user.getNome(), user.getNascimento(), user.getEmail(), user.getPassword(), user.getToken());
    }

    public static void delete(Connection conn, UsuarioTO user) throws Exception {
        StringBuilder sql = new StringBuilder();

        sql.append("DELETE FROM `Usuarios`");
        sql.append("  WHERE `idUsuario` = ?; ");

        Data.executeUpdate(conn, sql.toString(), user.getIdUsuario());
    }


    public static List<UsuarioTO> getAll(Connection conn) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT idUsuarios, Nome, Nascimento, Email, Password, Token, Active, Createdat, Expiredat");
        sql.append(" FROM usuarios");
        sql.append(" WHERE Active");

        try(ResultSet rs = Data.executeQuery(conn, sql.toString())){
            List<UsuarioTO> result = new ArrayList<>();
            while(rs.next()){
                UsuarioTO user =  setObject(rs);
                result.add(user);
            }
            return result;
        }
    }

    public static UsuarioTO auth(Connection conn, UsuarioTO user) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT `idUsuarios`, `Nome`, `Nascimento`, `Email`, `Password`, `Token`, `Active`, `Createdat`, `Expiredat`");
        sql.append(" FROM `Usuarios`");
        sql.append(" WHERE `Email` = ? AND `Password` = ? AND Active");

        try (ResultSet rs = Data.executeQuery(conn, sql.toString(), user.getEmail(), user.getPassword())) {
            if (rs.next()) {
                return setObject(rs);
            }
            return null;
        }
    }

    public static void updateToken(Connection c, UsuarioTO u) throws Exception {
        StringBuilder s = new StringBuilder();
        s.append(" update Usuarios ");
        s.append(" set Token = ?, Expiredat = ? ");
        s.append(" where idUsuarios = ? ");
        Data.executeUpdate(c, s.toString(), u.getToken(), u.getExpiredAt(), u.getIdUsuario());
    }

    private static UsuarioTO setObject(ResultSet rs) throws Exception {
        UsuarioTO user = new UsuarioTO();
        user.setIdUsuario(rs.getInt("idUsuarios"));
        user.setNome(rs.getString("Nome"));
        user.setNascimento(rs.getDate("Nascimento"));
        user.setEmail(rs.getString("Email"));
        user.setPassword(rs.getString("Password"));
        user.setToken(rs.getString("Token"));
        user.setActive(rs.getBoolean("Active"));
        user.setCreatedAt(rs.getTimestamp("Createdat"));
        user.setExpiredAt(rs.getTimestamp("Expiredat"));

        return user;
    }



    public static UsuarioTO getByToken(Connection conn, String token) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT `idUsuarios`, `Nome`, `Nascimento`, `Email`, `Password`, `Token`, `Active`, `Createdat`, `Expiredat`");
        sql.append(" FROM `Usuarios`");
        sql.append(" WHERE `Token` = ?");
        sql.append(" AND Active");

        try (ResultSet rs = Data.executeQuery(conn, sql.toString(), token)) {

            if (rs.next()) {
                return setObject(rs);
            }

            return null;
        }
    }
    public static UsuarioTO get(Connection conn, String id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT `idUsuarios`, `Nome`, `Nascimento`, `Email`, `Password`, `Token`, `Active`, `Createdat`, `Expiredat`");
        sql.append(" FROM `Usuarios`");
        sql.append(" WHERE `idUsuarios` = ?");
        sql.append(" AND Active");

        try (ResultSet rs = Data.executeQuery(conn, sql.toString(), id)) {
            if (rs.next()) {
                return setObject(rs);
            }
            return null;
        }
    }
}
