package dao;
import fw.Data;
import fw.DateTime;
import to.AmigosTO;
import to.BloqueadosTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class BloqueadosDAO {

    public static void insert(Connection c, BloqueadosTO model) throws Exception {

        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO `Bloqueados`(`idUsuario`, `Bloqueado`,`Created`) VALUES ");
        sql.append(" (?, ?, NOW()) ");

        Data.executeUpdate(c, sql.toString(), model.getIdUsuario(), model.getIdAmigo());

    }
    public static void delete(Connection c, BloqueadosTO model) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM `Bloqueados` WHERE `idBloqueados` = ?");

        Data.executeUpdate(c, sql.toString(), model.getIdAmigo());

    }

    // Obter uma unica postagem
    public static BloqueadosTO get(Connection c, BloqueadosTO model) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idAmigo`, `Create`, `idUsuario`, `Amigo` FROM `Bloqueados`");
        sql.append(" where ");
        sql.append(" idAmigo = ? ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), model.getIdAmigo())){

            if(rs.next()){

                BloqueadosTO result = new BloqueadosTO();

                result.setIdAmigo(rs.getInt("idAmigo"));
                result.setIdUsuario(rs.getInt("idUsuario"));
                result.setAmigo(rs.getInt("Amigo"));

                result.setCreate(rs.getDate("Create"));

                return result;

            }else{
                return null;
            }

        }
    }
    // obter todas as postagens
    public static List<BloqueadosTO> getAmigos(Connection c, int idUsuario) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idAmigo`, `Create`, `idUsuario`, `Amigo` FROM `Bloqueados` ");
        sql.append(" where ");
        sql.append(" idUsuario = ? ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), idUsuario)){

            List<BloqueadosTO> result = new ArrayList<>();

            while(rs.next()){
                BloqueadosTO model = new BloqueadosTO();

                model.setIdAmigo(rs.getInt("idAmigo"));
                model.setIdUsuario(rs.getInt("idUsuario"));
                model.setAmigo(rs.getInt("Amigo"));

                model.setCreate(rs.getDate("Create"));


                result.add(model);

            }

            return result;
        }
    }
}
