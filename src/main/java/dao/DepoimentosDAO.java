package dao;
import fw.Data;
import fw.DateTime;
import to.DepoimentoTO;
import to.PublicacaoTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DepoimentosDAO {

    public static void insert(Connection c, DepoimentoTO model) throws Exception {

        StringBuilder sql = new StringBuilder();

        sql.append(" INSERT INTO `Depoimentos`(`idUsuario`, `Comentario`) VALUES ");
        sql.append(" (?, ?) ");

        Data.executeUpdate(c, sql.toString(), model.getIdUsuario(), model.getComentario());

    }
    // Obter uma unica postagem
    public static DepoimentoTO get(Connection c, DepoimentoTO model) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idDepoimento`, `idUsuario`, `Comentario` FROM `Depoimentos` ");
        sql.append(" where ");
        sql.append(" idDepoimento = ? ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), model.getIdDepoimento())){

            if(rs.next()){

                DepoimentoTO result = new DepoimentoTO();

                result.setIdDepoimento(rs.getInt("idDepoimento"));
                result.setIdUsuario(rs.getInt("idUsuario"));
                result.setComentario(rs.getString("Comentario"));

                return result;

            }else{
                return null;
            }

        }
    }
    // obter todas as postagens
    public static List<DepoimentoTO> getUsuario(Connection c, int idUsuario) throws Exception {

        StringBuilder sql = new StringBuilder();

        sql.append(" SELECT `idDepoimento`, `idUsuario`, `Comentario` FROM `Depoimentos` ");
        sql.append(" where ");
        sql.append(" idUsuario = ? ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), idUsuario)){

            List<DepoimentoTO> result = new ArrayList<>();

            while(rs.next()){

                DepoimentoTO model = new DepoimentoTO();

                model.setIdDepoimento(rs.getInt("idDepoimento"));
                model.setIdUsuario(rs.getInt("idUsuario"));
                model.setComentario(rs.getString("Comentario"));

                result.add(model);

            }

            return result;
        }
    }
}
