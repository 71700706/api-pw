package to;

import java.util.Date;

public class AmigosTO {

    private int idAmigo;
    private int idUsuario;
    private int Amigo;
    private Date Create;

    public int getIdAmigo() {
        return idAmigo;
    }

    public void setIdAmigo(int idAmigo) {
        this.idAmigo = idAmigo;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getAmigo() {
        return Amigo;
    }

    public void setAmigo(int amigo) {
        Amigo = amigo;
    }

    public Date getCreate() {
        return Create;
    }

    public void setCreate(Date create) {
        Create = create;
    }
}
