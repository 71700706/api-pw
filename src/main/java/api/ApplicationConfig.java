package api;

import javax.ws.rs.core.Application;
import java.util.Set;


@javax.ws.rs.ApplicationPath("v1")
public class ApplicationConfig extends Application {


    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {

        resources.add(ServiceVersion.class);

        resources.add(ServiceUsuario.class);


        resources.add(ServicePublicacao.class);
        resources.add(ServiceAnexos.class);
        resources.add(ServiceCurtida.class);
        resources.add(ServiceComentario.class);

        resources.add(ServiceDepoimento.class);



    }

}
