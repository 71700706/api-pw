package bo;

import dao.AmigosDAO;
import dao.AnexosDAO;
import fw.Data;
import to.AmigosTO;
import to.AnexoTO;
import to.PublicacaoTO;

import java.sql.Connection;
import java.util.List;

public class AmigosBO {

    public static void insert(AmigosTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            AmigosDAO.insert(c, model);
        }
    }
    public static void delete(AmigosTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            AmigosDAO.delete(c, model);
        }
    }
    public static AmigosTO get(AmigosTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            return AmigosDAO.get(c, model);
        }
    }
    
    public static List<AmigosTO> getAmigos(int idAmigos) throws Exception {
        try (Connection c = Data.openConnection()) {
            return AmigosDAO.getAmigos(c, idAmigos);
        }
    }
}
