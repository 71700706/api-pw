package dao;
import fw.Data;
import fw.DateTime;
import to.AmigosTO;
import to.PublicacaoTO;
import to.UsuarioTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AmigosDAO {

    public static void insert(Connection c, AmigosTO model) throws Exception {

        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO `Amigos`(`idUsuario`, `Amigo`,`Create`) VALUES ");
        sql.append(" (?, ?, ?) ");

        Data.executeUpdate(c, sql.toString(), model.getIdUsuario(), model.getIdAmigo(), new DateTime());

    }
    public static void delete(Connection c, AmigosTO model) throws Exception {

        StringBuilder sql = new StringBuilder();

        sql.append("DELETE FROM `Amigos` WHERE `idAmigo` = ?");

        Data.executeUpdate(c, sql.toString(), model.getIdAmigo());

    }

    // Obter um unico amigo
    public static AmigosTO get(Connection c, AmigosTO model) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idAmigo`, `Create`, `idUsuario`, `Amigo` FROM `Amigos`");
        sql.append(" where ");
        sql.append(" idAmigo = ? ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), model.getIdAmigo())){

            if(rs.next()){

                AmigosTO result = new AmigosTO();

                result.setIdAmigo(rs.getInt("idAmigo"));
                result.setIdUsuario(rs.getInt("idUsuario"));
                result.setAmigo(rs.getInt("Amigo"));

                result.setCreate(rs.getDate("Create"));

                return result;

            }else{
                return null;
            }

        }
    }
    // obter todos os amigos
    public static List<AmigosTO> getAmigos(Connection c, int idUsuario) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT `idAmigo`, `Create`, `idUsuario`, `Amigo` FROM `Amigos` ");
        sql.append(" where idUsuario = ? ");

        try(ResultSet rs = Data.executeQuery(c, sql.toString(), idUsuario)){

            List<AmigosTO> result = new ArrayList<>();

            while(rs.next()){
                AmigosTO model = new AmigosTO();

                model.setIdAmigo(rs.getInt("idAmigo"));
                model.setIdUsuario(rs.getInt("idUsuario"));
                model.setAmigo(rs.getInt("Amigo"));

                model.setCreate(rs.getDate("Create"));

                result.add(model);

            }

            return result;
        }
    }
}
