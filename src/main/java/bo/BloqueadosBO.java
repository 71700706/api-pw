package bo;

import dao.AmigosDAO;
import dao.BloqueadosDAO;
import fw.Data;
import to.AmigosTO;
import to.BloqueadosTO;

import java.sql.Connection;
import java.util.List;

public class BloqueadosBO {

    public static void insert(BloqueadosTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            BloqueadosDAO.insert(c, model);
        }
    }
    public static void delete(BloqueadosTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            BloqueadosDAO.delete(c, model);
        }
    }
    public static BloqueadosTO get(BloqueadosTO model) throws Exception {
        try (Connection c = Data.openConnection()) {
            return BloqueadosDAO.get(c, model);
        }
    }
    public static List<BloqueadosTO> getComentarios(int idAmigos) throws Exception {
        try (Connection c = Data.openConnection()) {
            return BloqueadosDAO.getAmigos(c, idAmigos);
        }
    }
}
