package api;

import bo.ComentarioBO;
import bo.CurtidasBO;
import bo.PublicacaoBO;
import bo.UsuarioBO;
import to.ComentarioTO;
import to.PublicacaoTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

@Path("comentario")
public class ServiceComentario {

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @POST
    @Consumes("application/json;charset=utf-8")
    public void insert(@HeaderParam("token") String token,ComentarioTO model) throws Exception {
        if (UsuarioBO.isValid(token)) {
            ComentarioBO.insert(model);
            response.sendError(HttpServletResponse.SC_CREATED);

        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @DELETE
    @Path("{id}")
    public void delete(@HeaderParam("token") String token, @PathParam("id") int id) throws Exception {
        if (UsuarioBO.isValid(token)) {
            ComentarioBO.delete(id);
            response.sendError(HttpServletResponse.SC_OK);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @GET
    @Path("{id}")
    @Produces("application/json;charset=utf-8")
    public ComentarioTO get(@HeaderParam("token") String token, @PathParam("id") int id) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return ComentarioBO.get(id);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }

    @GET
    @Path("publicacao/{idPublicacao}")
    @Produces("application/json;charset=utf-8")
    public List<ComentarioTO> getUsuario(@HeaderParam("token") String token, @PathParam("idPublicacao") int idPublicacao) throws Exception {
        if (UsuarioBO.isValid(token)) {
            return ComentarioBO.getComentarios(idPublicacao);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
    }


}
